# GINJAE Is Not Just Another Editor
## State of the project
No code has been written.
## What is ginjae
### What ginjae wants to be
- A portable Work Environment (WE)
- A replacement for emacs without lisp
- Some sort of "Portable Window Manager"
- Good ?
ginjae should :
- be totally portable, 
- have only 1 config file, 
- be multi-platform,
### What ginjae does not want to be
- A replacement for vim. Stop trying to replace vim.
- Just Another text Editor.
- A terminal editor
## Structure of ginjae
Ginjae lives in a window containing a frame and a command prompt, similar to vim.
A frame can be :
1. A split frame
2. A typed frame
A split frame is a frame which is split into two or more frame horizontally or vertically.
A typed frame is a working space.
The type of a frame can be :
1. Text editor
2. Terminal
3. Canvas
4. PDF
5. webview
6. debugger
Types of frames can be subtyped, e.g. a python editor is a subtype of a code editor which is a subtype of a text editor.

Every action in Ginjae is done by calling a Python function, from moving a cursor to splitting a frame.
Actions are discoverable by using namespaces.
The function to split a frame into two frames vertically is `frame.split.vertical(2)`.
The function to move to cursor down is `cursor.move.vertical(1)`.
## What ginjae does differently
- ginjae does not try to fit eveything as a text buffer 
- ginjae uses python as its core language
- every extension for ginjae is written in python
